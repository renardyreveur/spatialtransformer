# Spatial Transformer Networks in Pytorch
### Jeehoon Kang

## Overview
This is a **PyTorch** Implementation of the [**Spatial Transformer Network**](https://papers.nips.cc/paper/5854-spatial-transformer-networks.pdf) [1] that was proposed by DeepMind.


*Spatial Transformer Networks (STN)* encompass various **learnable transformation modules** that can be placed inside pretty much anywhere within a Convolutional Neural Network. 
The differentiable module can be used with existing architectures **without extra labelling** or supervision.
The parameters will learn to reduce the overall loss given by the original structure!

## Motivation

There is a much simpler way of implementing this in PyTorch with the following functions
```
torch.nn.functional.affine_grid
torch.nn.functional.grid_sample
```
(refer to [Spatial Transformer Tutorial - PyTorch](https://pytorch.org/tutorials/intermediate/spatial_transformer_tutorial.html))


Yet currently (as of Aug 2020, PyTorch 1.6.0), it is unable to export this example into an **onnx model**.
On top of that, other transformations such as *Projective Transformation* or *Thin Plate Splines* are unavailable in the PyTorch library itself. 

This repository is a more 'from-scratch' approach towards the Spatial Transformer Networks, allowing it to be exported in to Onnx models.
 
  
## Dependencies
- **Python3**
- **Pytorch 1.6.0**
- **Onnxruntime 1.4.0**
- **OpenCV / Numpy** for sanity checks

## Usage

A localisation model is required as a parameter to the SpatialTransformer Class. <br>
The localisation model should be a model that regresses the transformation parameters for whatever parameter you're modelling with the STN module. <br>
For example, to model an affine transform, you need to regress 6 parameters! 
```python
import torch
import torch.nn as nn
import torch.nn.functional as F
from spatial_transformer import SpatialTransformer

class Localisation(nn.Module):
    def __init__(self):
        super().__init__()
        # Spatial transformer localization-network
        self.localization = nn.Sequential(
            nn.Conv2d(1, 8, kernel_size=7),
            nn.MaxPool2d(2, stride=2),
            nn.ReLU(True),
            nn.Conv2d(8, 10, kernel_size=5),
            nn.MaxPool2d(2, stride=2),
            nn.ReLU(True)
        )

        # Regressor for the 3 * 2 affine matrix
        self.fc_loc = nn.Sequential(
            nn.Linear(10 * 6 * 6, 32),
            nn.ReLU(True),
            nn.Linear(32, 3 * 2)
        )
        
        # Initialise the weights such that the regressor predicts identity to begin with
        self.fc_loc[2].weight.data.zero_()
        self.fc_loc[2].bias.data.copy_(torch.tensor([1, 0, 0, 0, 1, 0], dtype=torch.float))

    def forward(self, x):
        x = self.localization(x)
        x = x.view(-1, 10 * 6 * 6)
        theta = self.fc_loc(x)
        theta = theta.view(-1, 2, 3)
        return theta


class Model(nn.Module):
    def __init__(self, stn=False, num_classes=10):
        super().__init__()

        self.stn = stn

        if self.stn:
            self.localisation = Localisation()
            self.spatial_transformer = SpatialTransformer(self.localisation, (40, 40))

        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(20*7*7, 50)
        self.fc2 = nn.Linear(50, num_classes)

    def forward(self, x):
        if self.stn:
            x = self.spatial_transformer(x)

        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, 20*7*7)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)

        return x
```

> It is important to make the localisation network **regress the identity transform to begin with**.
>
> Also a **high learning rate might make the STN module diverge, use learning rate warmups or reduce the learning rate**.
>
> The sampler uses a vectorized operation which consumes a lot of memory. **When training use AMP if possible**
## Todo:
- [x] Implement Affine Transformation with STN with Bilinear sampling
- [ ] Add other sampling methods such as integer(nearest neighbor) sampling, etc.
- [ ] Implement Thin Plate Spline Transformation and other transformations
- [ ] Code Refactoring and Clean up


## References
[1] Max Jaderberg, Karen Simonyan, Andrew Zisserman, Koray Kavukcuoglu: “Spatial Transformer Networks”, 2015

[2] Shout out to [Kevin Zakka's awesome Tensorflow Implementation](https://github.com/kevinzakka/spatial-transformer-network)