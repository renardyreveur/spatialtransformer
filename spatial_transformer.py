# Title: Spatial Transformer Networks in Pytorch
# Author: Jeehoon Kang
# Date: 17th Sept. 2020
import torch
import torch.nn as nn
import torch.nn.functional as F

import stn_utils


class SpatialTransformer(nn.Module):
    def __init__(self, localisation, target_size, sampler='bilinear_sampler'):
        """
        Implements the Spatial Transformer Network with Pytorch
        Args:
            - localisation : Class implementing the nn.Module Class,
             a small network that regresses the transformation parameters
            - target_size: tuple, (width, height), size of the output map
            - sampler: string, sampler to use. Either ['integer_sampler', 'bilinear_sampler']

        Forward Returns:
            - Output map : of size target_size, transformed by the parameters derived from the localisation net
        """
        super().__init__()

        # Localisation net - regresses transformation parameters
        self.localisation = localisation

        # Create normalized grid on target shape and get point vectors
        self.width = target_size[0]
        self.height = target_size[1]
        x_t = (2 / (self.width - 1) * torch.arange(self.width) - 1).reshape(1, -1).repeat(self.height, 1).T.reshape(-1)
        y_t = (2 / (self.height - 1) * torch.arange(self.height) - 1).repeat(self.width)
        ones = torch.ones_like(x_t)
        self.target_grid = torch.stack([x_t, y_t, ones])
        # print("x_t: {}, y_t: {}".format(x_t.shape, y_t.shape))

        # Sampler
        self.sampler = getattr(stn_utils, sampler)

    def forward(self, in_x):
        # Regress transformation parameters
        theta = self.localisation(in_x)
        # print("Theta: ", theta.shape)

        # Get Source Grid
        num_batch = in_x.shape[0]
        sampling_grid = self.target_grid.reshape(1, 3, -1).repeat(num_batch, 1, 1).to(theta.device)

        # Transform target points by the transformation parameters
        source_grid = torch.bmm(theta, sampling_grid)\
            .reshape(num_batch, 2, self.width, self.height)\
            .permute(0, 3, 2, 1)

        pytorch_grid = F.affine_grid(theta, [num_batch, 1, self.width, self.height], align_corners=True)
        print(f"Max Difference between PyTorch Affine Grind: {torch.max(pytorch_grid - source_grid)}")

        # Bilinear sampling from source points to target points
        result_pytorch = F.grid_sample(in_x, source_grid, align_corners=True)
        result_custom = self.sampler(in_x, source_grid)
        # return result_custom
        return result_pytorch, result_custom
