import torch
from torch import nn
import torch.jit
import torch.nn.functional as F


def get_pixel(im, target_size, x_idx, y_idx):
    # Get shape
    batch, channel, height, width = im.shape
    # Flatten the indices for indexing
    x_idx = x_idx.reshape(batch, -1)
    y_idx = y_idx.reshape(batch, -1)
    # Get pixel value from source image
    # print(im[torch.arange(batch)[:,None], :, y_idx, ])
    p_val = im[torch.arange(batch)[:, None], :, y_idx, x_idx]
    return torch.clamp(p_val.reshape(batch, *target_size, channel).permute(0, 3, 1, 2), 0)


class bilinear_sampler(torch.autograd.Function):
    # it computes gradient wrt grid.
    @staticmethod
    def forward(ctx, img, grid):
        # Get input feature map shape
        batch, channel, height, width = img.shape

        # Separate x, y of sourced points
        x, y = grid[..., 0], grid[..., 1]

        # Get target feature map shape
        target_size = x.shape[-2:]

        # Rescale x and y to source shape (align corners = True)
        x = ((x + 1) / 2) * (width - 1)
        y = ((y + 1) / 2) * (height - 1)

        # Get nearest 4 corner points of rescaled x, y
        x0 = torch.floor(x).long()
        x1 = x0 + 1
        y0 = torch.floor(y).long()
        y1 = y0 + 1

        # For gradient masking
        x0_mask = (x0 > width - 1) + (x0 < 0)
        x1_mask = (x1 > width - 1) + (x1 < 0)
        y0_mask = (y0 > height - 1) + (y0 < 0)
        y1_mask = (x0 > height - 1) + (x0 < 0)

        # Bilinear sampling kernel
        # Ia ------ Ic
        # | wd | wb |
        # | -- x -- |
        # | wc | wa |
        # Ib ------ Id
        # Get Pixel values
        Ia = get_pixel(img, target_size, x0.clamp(0, width - 1), y0.clamp(0, height - 1))
        Ib = get_pixel(img, target_size, x0.clamp(0, width - 1), y1.clamp(0, height - 1))
        Ic = get_pixel(img, target_size, x1.clamp(0, width - 1), y0.clamp(0, height - 1))
        Id = get_pixel(img, target_size, x1.clamp(0, width - 1), y1.clamp(0, height - 1))

        # Mask out output pixel value to zero if sourced point out of bounds
        Ia[(x0_mask + y0_mask + x1_mask + y1_mask).unsqueeze(1)] = 0
        Ib[(x0_mask + y0_mask + x1_mask + y1_mask).unsqueeze(1)] = 0
        Ic[(x0_mask + y0_mask + x1_mask + y1_mask).unsqueeze(1)] = 0
        Id[(x0_mask + y0_mask + x1_mask + y1_mask).unsqueeze(1)] = 0

        # Get respective weights for the bilinear kernel
        wa = ((x1 - x) * (y1 - y)).unsqueeze(1)
        wb = ((x1 - x) * (y - y0)).unsqueeze(1)
        wc = ((x - x0) * (y1 - y)).unsqueeze(1)
        wd = ((x - x0) * (y - y0)).unsqueeze(1)

        # Get sampled pixel value
        out_value = (wa * Ia) + (wb * Ib) + (wc * Ic) + (wd * Id)

        ctx.save_for_backward(x, y, x0, x1, y0, y1, wa, wb, wc, wd, Ia, Ib, Ic, Id)
        return out_value

    @staticmethod
    def backward(ctx, grad_output):
        # print(grad_output.shape)
        x, y, x0, x1, y0, y1, wa, wb, wc, wd, Ia, Ib, Ic, Id = ctx.saved_tensors
        height, width = x.shape[-2:]
        x = x.unsqueeze(1)
        y = y.unsqueeze(1)
        x0 = x0.unsqueeze(1)
        x1 = x1.unsqueeze(1)
        y0 = y0.unsqueeze(1)
        y1 = y1.unsqueeze(1)
        wa = wa.unsqueeze(1)
        wb = wb.unsqueeze(1)
        wc = wc.unsqueeze(1)
        wd = wd.unsqueeze(1)
        partial_u = torch.ones_like(x)
        partial_u = partial_u * (x0 > 0) * (y0 > 0) * (x1 < width) * (y1 < height)
        # print(f'pp shape: {(x0 == 0).shape}')
        # print(f'pps shape: {wd.shape}')
        partial_u[x0 == 0] = wd[x0 == 0] + wc[x0 == 0]  ## wd+wc
        # print(f'pp shape: {partial_u.shape}')
        partial_u[x1 == width] = wb[x1 == width] + wa[x1 == width]
        partial_u[y0 == 0] = wd[y0 == 0] + wc[y0 == 0]  ## wd+wc
        partial_u[y1 == height] = wb[y1 == height] + wa[y1 == height]
        partial_x = (Ic - Ia) * (torch.ones_like(y) - torch.abs(y - y0)) + (Id - Ib) * (
                torch.ones_like(y) - torch.abs(y - y1))
        partial_y = (Ib - Ia) * (torch.ones_like(x) - torch.abs(x - x0)) + (Id - Ic) * (
                torch.ones_like(x) - torch.abs(x - x1))
        # print(f'partial_x: {partial_x}') ## (500,500,40,40)
        # print(f'partial_x: {partial_x}')
        # print(f'partial_y: {partial_y}')
        # print(f'partial_u: {partial_u}')
        # print(torch.sum(grad_output))
        return grad_output * partial_u.squeeze(1), \
               torch.cat([grad_output * partial_x.squeeze(1), grad_output * partial_y.squeeze(1)], dim=-1)
        # return partial_u.squeeze(1), partial_x.squeeze(1), partial_y.squeeze(1)


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(980, 50)
        self.fc2 = nn.Linear(50, 10)
        # Spatial transformer localization-network
        self.localization = nn.Sequential(
            nn.Conv2d(1, 8, kernel_size=3),
            nn.MaxPool2d(2, stride=2),
            nn.ReLU(True),
            nn.Conv2d(8, 1, kernel_size=3),
            nn.MaxPool2d(2, stride=2),
            nn.ReLU(True)
        )
        # Regressor for the 3 * 2 affine matrix
        self.fc_loc = nn.Sequential(
            nn.Linear(4, 32),
            nn.ReLU(True),
            nn.Linear(32, 6)
        )
        # Initialize the weights/bias with identity transformation
        self.fc_loc[2].weight.data.zero_()
        self.fc_loc[2].bias.data.copy_(torch.tensor([1, 0, 0, 0, 1, 0], dtype=torch.float))

    # Spatial transformer network forward function
    def stn(self, x):
        # with torch.no_grad():
        xs = self.localization(x)
        xs = xs.view(xs.shape[0], -1)
        # print(f'xs.shape before fc_loc: {xs.shape}')
        # theta = torch.tensor([1, 0, 0, 0, 1, 0], dtype=torch.float, device=xs.device)
        theta = self.fc_loc(xs)
        theta = theta.view(-1, 2, 3)  # .repeat(x.shape[0],1,1)
        # print(theta)
        grid = F.affine_grid(theta, x.size())
        #
        # grid[..., 0] += 1
        # grid[..., 0] *= grid.shape[1]//2
        #
        # grid[..., 1] += 1
        # grid[..., 1] *= grid.shape[2]//2
        # print(grid)
        ## 28*28*1
        bilinear_sampler = Sampler()
        x = bilinear_sampler.apply(x, grid.permute(3, 0, 1, 2)[0], grid.permute(3, 0, 1, 2)[1])
        # print(torch.sum(x))
        # x = F.grid_sample(x, grid)
        return x

    # @profile_every(1)
    def forward(self, x):
        # transform the input
        x = self.stn(x)
        # Perform the usual forward pass
        # print(f'before forward x.shape: {x.shape}')
        x = F.relu(self.conv1(x), 2)
        # print(f'x.shape: {x.shape}')
        x = F.relu(self.conv2_drop(self.conv2(x)), 2)
        # print(f'x.shape: {x.shape}')
        x = x.view(x.shape[0], -1)
        # print(f'before fc1 forward pass: {x.shape}')
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        # print(f'x.shape: {x.shape}')
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)
