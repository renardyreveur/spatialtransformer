import torch


def get_pixel(im, target_size, x_idx, y_idx):
    # Get shape
    batch, channel, height, width = im.shape

    # Flatten the indices for indexing
    x_idx = x_idx.reshape(batch, -1).clamp(0, width - 1)
    y_idx = y_idx.reshape(batch, -1).clamp(0, height - 1)

    # Get pixel value from source image
    p_val = im[torch.arange(batch)[:, None], :, y_idx, x_idx]

    return torch.clamp(p_val.reshape(batch, *target_size, channel).permute(0, 3, 1, 2), 0)


def bilinear_sampler(img, grid):

    # Get input feature map shape
    batch, channel, height, width = img.shape

    # Separate x, y of sourced points
    x, y = grid[..., 0], grid[..., 1]

    # Get target feature map shape
    target_size = x.shape[-2:]

    # Rescale x and y to source shape (align corners = True)
    x = ((x + 1) / 2) * (width - 1)
    y = ((y + 1) / 2) * (height - 1)

    # Get nearest 4 corner points of rescaled x, y
    x0 = torch.floor(x).long()
    x1 = x0 + 1
    y0 = torch.floor(y).long()
    y1 = y0 + 1

    # For gradient masking
    masks = [(x0 > width - 1), (x0 < 0),
             (x1 > width - 1), (x1 < 0),
             (y0 > height - 1), (y0 < 0),
             (x0 > height - 1), (x0 < 0)]

    # Bilinear sampling kernel
    # Ia ------ Ic
    # | wd | wb |
    # | -- x -- |
    # | wc | wa |
    # Ib ------ Id

    # Get Pixel values
    Ia = get_pixel(img, target_size, x0, y0)
    Ib = get_pixel(img, target_size, x0, y1)
    Ic = get_pixel(img, target_size, x1, y0)
    Id = get_pixel(img, target_size, x1, y1)

    # Mask out output pixel value to zero if sourced point out of bounds
    for m in masks:
        Ia = torch.masked_fill(Ia, m, 0)
        Ib = torch.masked_fill(Ib, m, 0)
        Ic = torch.masked_fill(Ic, m, 0)
        Id = torch.masked_fill(Id, m, 0)

    # Get respective weights for the bilinear kernel
    wa = ((x1 - x) * (y1 - y)).unsqueeze(1)
    wb = ((x1 - x) * (y - y0)).unsqueeze(1)
    wc = ((x - x0) * (y1 - y)).unsqueeze(1)
    wd = ((x - x0) * (y - y0)).unsqueeze(1)

    # Get sampled pixel value
    out_value = (wa * Ia) + (wb * Ib) + (wc * Ic) + (wd * Id)

    return out_value
