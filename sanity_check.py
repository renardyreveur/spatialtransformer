import torch
import math
import cv2
import os
import numpy as np

from spatial_transformer import SpatialTransformer


# Test Localisation Network that outputs either a rotation matrix or an identity matrix
def test_localisation(img, transform="rotate"):
    assert transform in ["rotate", "identity"]

    with torch.no_grad():
        if transform == "rotate":
            degree = 45
            rad = (degree * math.pi)/180
            res = torch.tensor([
                [math.cos(rad), -math.sin(rad), 0],
                [math.sin(rad), math.cos(rad), 0]
            ])
        elif transform == "identity":
            res = torch.tensor([
                [1, 0, 0],
                [0, 1, 0]
            ], requires_grad=False).float()

    localisation_params = res.reshape(1, 2, 3).repeat(img.shape[0], 1, 1)
    return localisation_params


if __name__ == "__main__":
    # Read testing image
    root_dir = './data'
    out_dir = './result'
    file_names = os.listdir(root_dir)

    images = []
    for file in file_names:
        images.append(cv2.resize(cv2.imread(os.path.join(root_dir, file), cv2.IMREAD_GRAYSCALE), (600, 600)))

    # Make it into tensor for input
    in_tensor = torch.from_numpy(np.stack(images)).unsqueeze(1).float() / 255

    # Forward pass of Model
    model = SpatialTransformer(localisation=test_localisation, target_size=(600, 600))
    out_pytorch, out_custom = model(in_tensor)

    for i in range(len(out_pytorch)):
        result_pytorch = (out_pytorch[i].permute(1, 2, 0).numpy() * 255).astype('uint8')
        result_custom = (out_custom[i].permute(1, 2, 0).numpy() * 255).astype('uint8')
        diff = np.abs(result_pytorch - result_custom)
        cv2.imshow("idd", diff)
        cv2.waitKey(0)
        # Save output to file
        cv2.imwrite(os.path.join(out_dir, "out_pytorch_{}.jpg".format(i)), result_pytorch)
        cv2.imwrite(os.path.join(out_dir, "out_custom_{}.jpg".format(i)), result_custom)
