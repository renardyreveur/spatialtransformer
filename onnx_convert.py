import torch
import numpy as np
import onnxruntime
from spatial_transformer import SpatialTransformer
from sanity_check import test_localisation


def to_numpy(tensor):
    return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()


# Define Spatial Transformer Network Model
model = SpatialTransformer(test_localisation, (40, 40), "bilinear_sampler")

# Model Eval mode
model.eval()

# Create Dummy Input and Output for Testing
dummy = torch.randn(1, 1, 40, 40, requires_grad=True)
torch_out = model(dummy)

# Output onnx file name
onnx_out = "stn.onnx"

# Exporting the Model
torch.onnx.export(model,                                    # model to export
                  (dummy,),                                 # Dummy Input
                  onnx_out,                                 # Where to Save Model
                  export_params=True,                       # Store trained parameter weights
                  do_constant_folding=True,                 # Optimization
                  opset_version=12,
                  input_names=['input'],
                  output_names=['output'],
                  # dynamic_axes={'input': {2: 'height', 3: 'width'}, 'output': {2: 'height', 3: 'width'}}
                  )

print("Exported model to ONNX file")

# Test whether outputs are same within reasonable distance
ort_session = onnxruntime.InferenceSession(onnx_out)

# Compute ONNX Runtime output prediction
ort_inputs = {ort_session.get_inputs()[0].name: to_numpy(dummy)}
ort_outs = ort_session.run(None, ort_inputs)

# compare ONNX Runtime and PyTorch results
np.testing.assert_allclose(to_numpy(torch_out), ort_outs[0], rtol=1e-03, atol=1e-05)
print("Exported model has been tested with ONNXRuntime, and the result looks good!")
